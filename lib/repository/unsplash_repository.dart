import 'dart:convert';
import 'package:http/http.dart' as http;

class UnsplashRepository {
  static const String _baseUrl = 'https://api.unsplash.com/';
  static const String numberPerPage = "15";
  static const String clientId =
      '/?client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9&per_page=';

  Uri fullUrl(String endPoint) =>
      Uri.parse(_baseUrl + endPoint + clientId + numberPerPage);

  Future getPhotos(String endpoint) async {
    final response = await http.get(fullUrl(endpoint));

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print("Http error: ${response.statusCode}");

      return [];
    }
  }
}
