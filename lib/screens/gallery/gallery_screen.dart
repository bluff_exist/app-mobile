import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../constants/constant.dart';
import '../../providers/galleryProvider.dart';
import '../../widgets/gallery/photos_list.dart';

class GalleryScreen extends StatelessWidget {
  static const routeName = '/gallery';

  @override
  Widget build(BuildContext context) {
    Provider.of<GalleryProvider>(context, listen: false).getPhotos();

    return Scaffold(
      appBar: AppBar(
        title: Text('Gallery'),
        backgroundColor: kAppBackgroundColor,
      ),
      body: PhotosList(),
    );
  }
}
