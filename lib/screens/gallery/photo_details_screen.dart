import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../constants/constant.dart';
import '../../providers/galleryProvider.dart';

class PhotoDetailsScreen extends StatelessWidget {
  static const routeName = '/gallery/photo';

  @override
  Widget build(BuildContext context) {
    GalleryProvider _galleryProvider = Provider.of<GalleryProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Photo Details'),
        backgroundColor: kAppBackgroundColor,
      ),
        body: Image.network(
          _galleryProvider.photo.urlRegular,
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
    );
  }
}