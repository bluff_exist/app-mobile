import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/galleryProvider.dart';
import 'screens/gallery/gallery_screen.dart';
import 'screens/gallery/photo_details_screen.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => GalleryProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Gallery',
        initialRoute: GalleryScreen.routeName,
        routes: {
          GalleryScreen.routeName: (context) => GalleryScreen(),
          PhotoDetailsScreen.routeName: (context) => PhotoDetailsScreen(),
        },
      ),
    );
  }
}
