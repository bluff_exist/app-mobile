import 'package:flutter/widgets.dart';

import '../models/photo.dart';
import '../services/gallery_service.dart';

class GalleryProvider with ChangeNotifier {
  final galleryService = GalleryService();

  List<Photo> _photosList = [];
  late Photo _photo;

  Photo get photo => _photo;
  List<Photo> get photos => _photosList;

  void getPhotos() async {
    _photosList = await galleryService.getPhotos();
    notifyListeners();
  }

  void setPhoto(Photo photo) {
    _photo = photo;
    notifyListeners();
  }
}