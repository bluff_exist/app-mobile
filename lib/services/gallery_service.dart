import '../models/photo.dart';
import '../repository/unsplash_repository.dart';

class GalleryService {
  final UnsplashRepository _unsplashRepository = UnsplashRepository();

  Future<List<Photo>> getPhotos() async {
    final response = await _unsplashRepository.getPhotos("photos");

    return (response as List).map((e) => Photo.fromJson(e)).toList();
  }
}
