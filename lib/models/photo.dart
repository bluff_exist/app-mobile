class Photo {
  String name;
  String author;
  String urlThumb;
  String urlRegular;

  Photo({
    required this.name,
    required this.author,
    required this.urlThumb,
    required this.urlRegular,
  });

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        name: json["description"] ?? '',
        author: json["user"]["name"],
        urlThumb: json["urls"]["thumb"],
        urlRegular: json["urls"]["regular"],
      );
}