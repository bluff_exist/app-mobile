import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_project/widgets/gallery/photo_tile.dart';

import '../../models/photo.dart';
import '../../providers/galleryProvider.dart';

class PhotosList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GalleryProvider _galleryProvider = Provider.of<GalleryProvider>(context);

    return ListView.separated(
      itemCount: _galleryProvider.photos.length,
      itemBuilder: (context, index) {
        Photo photo = _galleryProvider.photos[index];
        return PhotoTile(photo);
      },
      separatorBuilder: (BuildContext context, int index) {
        return const Padding(
          padding: EdgeInsets.only(left: 16.0),
          child: Divider(height: 0.5, color: Colors.black26),
        );
      },
    );
  }
}