import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/photo.dart';
import '../../providers/galleryProvider.dart';
import '../../screens/gallery/photo_details_screen.dart';

class PhotoTile extends StatelessWidget {
  final Photo photo;

  PhotoTile(this.photo);

  @override
  Widget build(BuildContext context) {
    GalleryProvider _galleryProvider = Provider.of<GalleryProvider>(context);

    return ListTile(
      key: key,
      title: Text(photo.author),
      subtitle: Text(photo.name),
      leading: ConstrainedBox(
        constraints: const BoxConstraints(
          maxWidth: 64,
          maxHeight: 64,
        ),
        child: Image.network(photo.urlThumb),
      ),
      onTap: () {
        _galleryProvider.setPhoto(photo);
        Navigator.pushNamed(context, PhotoDetailsScreen.routeName);
      },
    );
  }
}
